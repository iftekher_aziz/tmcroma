<?php
/**
 * Child-Theme functions and definitions
 */

function shaha_child_enqueue_styles() {
    wp_enqueue_style( 'shaha-parent-style', get_template_directory_uri() . '/style.css' );
	    if (is_rtl()) {
 			wp_enqueue_style( 'shaha-parent-rtl-style', get_template_directory_uri(). '/rtl.css' );
    }
}
add_action( 'wp_enqueue_scripts', 'shaha_child_enqueue_styles' );

function shaha_child_add_footer_styles() {	
	wp_enqueue_style( 'shaha-parent-style-rtl', get_stylesheet_directory_uri() . '/rtl.css' );
};

if( is_rtl() ) {
    add_action( 'get_footer', 'shaha_child_add_footer_styles' );
}